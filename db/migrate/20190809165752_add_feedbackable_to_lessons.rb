class AddFeedbackableToLessons < ActiveRecord::Migration[5.2]
  def change
    add_reference :lessons, :feedbackable, polymorphic: true, index: true
  end
end
