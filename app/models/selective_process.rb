class SelectiveProcess < ApplicationRecord
    has_many :courses

    has_many :participations
    has_many :users through: :participations
end
