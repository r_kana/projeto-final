Rails.application.routes.draw do
  resources :submissions
  resources :feedbacks
  resources :collaborations
  resources :participations
  resources :presences
  resources :users
  resources :activities
  resources :materials
  resources :lessons
  resources :courses
  resources :selective_processes
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
