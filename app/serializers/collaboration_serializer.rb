class CollaborationSerializer < ActiveModel::Serializer
  attributes :id, :kind
  has_one :user
  has_one :course
end
