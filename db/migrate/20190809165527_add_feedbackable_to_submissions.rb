class AddFeedbackableToSubmissions < ActiveRecord::Migration[5.2]
  def change
    add_reference :submissions, :feedbackable, polymorphic: true, index: true
  end
end
