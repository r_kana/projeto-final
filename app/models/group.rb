class Group < ApplicationRecord
  belongs_to :activity
  has_many :submissions, as: :submissible

  has_many :user_groups
  has_many :users through: :user_groups
end
