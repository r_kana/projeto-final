class Course < ApplicationRecord
    has_many :lessons
    has_many :materials
    belongs_to :selective_process

    has_many :collaborations
    has_many :users through: :collaborations

end
