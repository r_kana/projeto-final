# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_09_171543) do

  create_table "activities", force: :cascade do |t|
    t.string "description"
    t.integer "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_activities_on_lesson_id"
  end

  create_table "collaborations", force: :cascade do |t|
    t.integer "kind"
    t.integer "user_id"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_collaborations_on_course_id"
    t.index ["user_id"], name: "index_collaborations_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.text "classPlan"
    t.string "environment_setting"
    t.integer "selective_process_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["selective_process_id"], name: "index_courses_on_selective_process_id"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.text "content"
    t.integer "user_id"
    t.integer "feedbackable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feedbackable_id"], name: "index_feedbacks_on_feedbackable_id"
    t.index ["user_id"], name: "index_feedbacks_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.integer "activity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "submissible_type"
    t.integer "submissible_id"
    t.index ["activity_id"], name: "index_groups_on_activity_id"
    t.index ["submissible_type", "submissible_id"], name: "index_groups_on_submissible_type_and_submissible_id"
  end

  create_table "lesson_subjects", force: :cascade do |t|
    t.integer "lesson_id"
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_lesson_subjects_on_lesson_id"
    t.index ["subject_id"], name: "index_lesson_subjects_on_subject_id"
  end

  create_table "lessons", force: :cascade do |t|
    t.date "day"
    t.time "starTime"
    t.time "duration"
    t.string "subject"
    t.string "description"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "feedbackable_type"
    t.integer "feedbackable_id"
    t.index ["course_id"], name: "index_lessons_on_course_id"
    t.index ["feedbackable_type", "feedbackable_id"], name: "index_lessons_on_feedbackable_type_and_feedbackable_id"
  end

  create_table "materials", force: :cascade do |t|
    t.string "description"
    t.string "link"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_materials_on_course_id"
  end

  create_table "participations", force: :cascade do |t|
    t.integer "situation"
    t.integer "user_id"
    t.integer "selective_process_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["selective_process_id"], name: "index_participations_on_selective_process_id"
    t.index ["user_id"], name: "index_participations_on_user_id"
  end

  create_table "presences", force: :cascade do |t|
    t.integer "status"
    t.integer "performance"
    t.string "observation"
    t.integer "minutes_late"
    t.integer "user_id"
    t.integer "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_presences_on_lesson_id"
    t.index ["user_id"], name: "index_presences_on_user_id"
  end

  create_table "selective_processes", force: :cascade do |t|
    t.integer "year"
    t.integer "school_term"
    t.date "start_date"
    t.date "finish_date"
    t.date "registration_deadline"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subjects", force: :cascade do |t|
    t.string "subject"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "submissions", force: :cascade do |t|
    t.integer "status"
    t.string "link"
    t.integer "performance"
    t.text "observation"
    t.integer "activity_id"
    t.integer "submissible_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "feedbackable_type"
    t.integer "feedbackable_id"
    t.index ["activity_id"], name: "index_submissions_on_activity_id"
    t.index ["feedbackable_type", "feedbackable_id"], name: "index_submissions_on_feedbackable_type_and_feedbackable_id"
    t.index ["submissible_id"], name: "index_submissions_on_submissible_id"
  end

  create_table "user_groups", force: :cascade do |t|
    t.integer "user_id"
    t.integer "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_user_groups_on_group_id"
    t.index ["user_id"], name: "index_user_groups_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.boolean "admin"
    t.string "name"
    t.string "email"
    t.string "password"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "submissible_type"
    t.integer "submissible_id"
    t.index ["submissible_type", "submissible_id"], name: "index_users_on_submissible_type_and_submissible_id"
  end

end
