class Submission < ApplicationRecord
  belongs_to :activity
  belongs_to :submissible, polymorphic: true
  has_many :feedbacks, as: :feedbackable
end
