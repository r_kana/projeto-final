class PresenceSerializer < ActiveModel::Serializer
  attributes :id, :status, :performance, :observation, :minutes_late
  has_one :user
  has_one :lesson
end
