class Activity < ApplicationRecord
  has_many :groups
  has_many :submissions
  belongs_to :lesson
end
