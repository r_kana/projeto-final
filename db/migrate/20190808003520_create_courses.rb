class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :name
      t.text :classPlan
      t.string :environmentSetting
      t.references :selective_process, foreign_key: true

      t.timestamps
    end
  end
end
