class AddSubmissibleToGroup < ActiveRecord::Migration[5.2]
  def change
    add_reference :groups, :submissible, polymorphic: true, index: true
  end
end
