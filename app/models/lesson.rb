class Lesson < ApplicationRecord
    has_many :lesson_subjects
    has_many :subjects, through: :lesson_subjects
    
    has_many :activities

    belongs_to :course

    has_many :presences
    has_many :users through: :presences

    has_many :feedbacks, as: :feedbackable
end
