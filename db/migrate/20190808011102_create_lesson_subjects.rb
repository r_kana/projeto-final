class CreateLessonSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :lesson_subjects do |t|
      t.references :lesson, foreign_key: true
      t.references :subject, foreign_key: true

      t.timestamps
    end
  end
end
