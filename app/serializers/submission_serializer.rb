class SubmissionSerializer < ActiveModel::Serializer
  attributes :id, :status, :link, :performance, :observation
  has_one :activity
  has_one :submissible
  has_many :feedbacks as: :feedbackable
end
