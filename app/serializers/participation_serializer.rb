class ParticipationSerializer < ActiveModel::Serializer
  attributes :id, :situation
  has_one :user
  has_one :selectiveProcess
end
