class UserSerializer < ActiveModel::Serializer
  attributes :id, :admin, :name, :email, :password, :kind

  has_many :submissions as: :submissible

  has_many :participations
  has_many :selective_processes through: :participations

  has_many :user_groups
  has_many :groups through: :user_groups

  has_many :presences
  has_many :lessons through: :presences

  has_many :feedbacks

  has_many :collaborations 
  has_many :courses through: :collaborations
end
