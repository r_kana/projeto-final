class FixCourseColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :courses, :selective_process_id, :selectiveProcess_id
  end
end
