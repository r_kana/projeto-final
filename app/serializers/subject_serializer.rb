class SubjectSerializer < ActiveModel::Serializer
  attributes :id, :subject, :description

  has_many :lesson_subjects
  has_many :lessons trhough: :lesson_subjects
end