class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :classPlan, :environmentSetting

  has_one :selective_process

  has_many :materials

  has_many :collaborations 
  has_many :users through: :collaborations

  has_many :lessons
end
