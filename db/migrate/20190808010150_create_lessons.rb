class CreateLessons < ActiveRecord::Migration[5.2]
  def change
    create_table :lessons do |t|
      t.date :day
      t.time :starTime
      t.time :duration
      t.string :subject
      t.string :description
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
