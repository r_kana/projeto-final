class AddSubmissibleToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :submissible, polymorphic: true, index: true
  end
end
