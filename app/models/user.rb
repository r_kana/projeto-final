class User < ApplicationRecord
    has_many :presences
    has_many :lessons through: :presences

    has_many :collaborations
    has_many :courses through: :collaborations

    has_many :feedbacks

    has_many :submissions, as: :submissible

    has_many :user_groups
    has_many :groups through: :user_groups

    has_many :participations
    has_many :selective_processes through: :users
end
