class EditParticipationColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :participations, :selectiveProcess_id, :selective_process_id
  end
end
