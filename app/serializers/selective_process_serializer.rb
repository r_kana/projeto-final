class SelectiveProcessSerializer < ActiveModel::Serializer
  attributes :id, :year, :schoolTerm, :startDate, :registrationDeadline, :finishDate

  has_many :courses

  has_many :participations
  has_many :users through: :participations
end
