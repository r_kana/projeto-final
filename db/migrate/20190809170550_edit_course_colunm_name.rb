class EditCourseColunmName < ActiveRecord::Migration[5.2]
  def change
    rename_column :courses, :environmentSetting, :environment_setting
    rename_column :courses, :selectiveProcess_id, :selective_process_id
  end
end
