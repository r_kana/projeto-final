class EditSelectiveProcessColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :selective_processes, :schoolTerm, :school_term
    rename_column :selective_processes, :startDate, :start_date
    rename_column :selective_processes, :finishDate, :finish_date
    rename_column :selective_processes, :registrationDeadline, :registration_deadline
  end
end
