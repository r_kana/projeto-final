class LessonSerializer < ActiveModel::Serializer
  attributes :id, :day, :starTime, :duration, :subject :description

  has_one :course

  has_many :presences
  has_many :users through: :presences

  has_many :feedbacks as: :feedbackable

  has_many :activities

  has_many :lesson_subjects
  has_many :subjects through: :lesson_subjects
end

