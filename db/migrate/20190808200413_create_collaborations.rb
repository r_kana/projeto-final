class CreateCollaborations < ActiveRecord::Migration[5.2]
  def change
    create_table :collaborations do |t|
      t.integer :kind
      t.references :user
      t.references :course

      t.timestamps
    end
  end
end
